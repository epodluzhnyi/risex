<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * JWT authorisation and authentication
 */
Route::post('login', 'AuthController@login')->name('api.login');
Route::get('token/refresh', 'AuthController@refreshToken')->name('api.token.refresh');

Route::middleware(['jwt.auth'])->group(function() {

    Route::resource('transactions', 'TransactionsController')->except(['create', 'edit']);

    Route::prefix('user')->group(function() {
        Route::get('transactions', 'UserController@transactions')->name('user.transactions');
        Route::get('balance/{currency?}', 'UserController@balance')->name('user.balance');
    });

});

//Route::middleware('')->get('/user', function (Request $request) {
//    return $request->user();
//});
