<?php

namespace App\DTO;

use ArrayAccess;

/**
 * Class AbstractDTO
 * @package App\DTO
 */
abstract class AbstractDTO implements ArrayAccess
{

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * @var array
     */
    protected $attribute_values = [];

    /**
     * @param $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        if ($attribute == 'attribute_values') {
            foreach ($this->attributes as $attribute) {
                if (!isset($this->attribute_values[$attribute])) {
                    $this->attribute_values[$attribute] = null;
                }
            }
            return $this->attribute_values;
        }

        if ($attribute == 'attributes') {
            return $this->attributes;
        }

        return $this->attribute_values[$attribute] ?? null;
    }

    /**
     * @param $attribute
     * @param $value
     */
    public function __set($attribute, $value)
    {
        $this->offsetSet($attribute, $value);
    }

    /**
     * @param array $data
     * @return $this
     */
    public function fill(array $data)
    {
        foreach ($data as $key => $value) {
            if ($this->isAvailableAttribute($key)) {
                $this->attribute_values[$key] = $value;
            }
        }
        return $this;
    }

    /**
     * @param mixed $attribute
     * @param mixed $value
     */
    public function offsetSet($attribute, $value) {
        if ($this->isAvailableAttribute($attribute)) {
            $this->attribute_values[$attribute] = $value;
        }
    }

    /**
     * @param mixed $attribute
     * @return bool
     */
    public function offsetExists($attribute) {
        return isset($this->attributes[$attribute]);
    }

    /**
     * @param mixed $attribute
     */
    public function offsetUnset($attribute) {
        unset($this->attribute_values[$attribute]);
    }

    /**
     * @param mixed $attribute
     * @return mixed|null
     */
    public function offsetGet($attribute) {
        return $this->$attribute;
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return array_merge($this->attributes, $this->attribute_values);
    }

    /**
     * @return string
     */
    public function toJson() : string
    {
        return \json_encode($this->toArray());
    }

    /**
     * @param string $attribute
     * @return bool
     */
    protected function isAvailableAttribute(string $attribute) : bool
    {
        $attributes = array_flip($this->attributes);
        return isset($attributes[$attribute]);
    }

}