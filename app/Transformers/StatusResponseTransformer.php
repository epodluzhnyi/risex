<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class StatusResponseTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform()
    {
        return [
            //
        ];
    }
}
