<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

/**
 * Class AmountTransformer
 * @package App\Transformers
 */
class AmountTransformer extends TransformerAbstract
{

    /**
     * @param float $amount
     * @return array
     */
    public function transform(float $amount)
    {
        return [
            'amount' => (float)$amount,
        ];
    }

}
