<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

/**
 * Class JwtTransformer
 * @package App\Transformers
 */
class JwtTransformer extends TransformerAbstract
{

    /**
     * @param string $token
     * @return array
     */
    public function transform(string $token) : array
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
        ];
    }

}
