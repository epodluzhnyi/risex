<?php

namespace App\Transformers;

use Illuminate\Validation\ValidationException;
use League\Fractal\TransformerAbstract;

/**
 * Class ApiErrorTransformer
 * @package App\Transformers
 */
class ApiErrorTransformer extends TransformerAbstract
{

    /**
     * @param \Exception $exception
     * @return array
     */
    public function transform(\Exception $exception)
    {
        $status_code = 500;

        if ($exception instanceof ValidationException) {
            $status_code = 400;
        }

        return [
            'status' => $status_code,
            'message' => $exception->getMessage(),
        ];
    }

}
