<?php

namespace App\Transformers;

use App\Entities\User;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

/**
 * Class UserTransformer
 * @package App\Transformers
 */
class UserTransformer extends TransformerAbstract
{

    /**
     * @var array
     */
    protected $availableIncludes = [
        'transactions',
    ];

    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user) : array
    {
        return [
            'id'            => $user->getKey(),
            'name'          => $user->getAttribute('name'),
            'email'         => $user->getAttribute('email'),
            'created_at'    => $user->getAttribute('created_at')->timestamp,
            'updated_at'    => $user->getAttribute('updated_at')->timestamp,
        ];
    }

    /**
     * @param User $user
     * @return Collection
     */
    public function includeTransactions(User $user) : Collection
    {
        return $this->collection($user->transactions, new TransactionTransformer);
    }

}
