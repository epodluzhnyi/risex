<?php

namespace App\Transformers;

use App\Entities\Transaction;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * Class TransactionTransformer
 * @package App\Transformers
 */
class TransactionTransformer extends TransformerAbstract
{

    /**
     * @var array
     */
    protected $defaultIncludes = [
        'author'
    ];

    /**
     * @param Transaction $transaction
     * @return array
     */
    public function transform(Transaction $transaction) : array
    {
        return [
            'id'            => $transaction->getKey(),
            'amount'        => $transaction->getAttribute('amount'),
            'title'         => $transaction->getAttribute('title'),
            'created_at'    => $transaction->getAttribute('created_at')->timestamp,
            'updated_at'    => $transaction->getAttribute('updated_at')->timestamp,
        ];
    }

    /**
     * @param Transaction $transaction
     * @return \League\Fractal\Resource\Item
     */
    public function includeAuthor(Transaction $transaction) : Item
    {
        return $this->item($transaction->author, new UserTransformer());
    }

}
