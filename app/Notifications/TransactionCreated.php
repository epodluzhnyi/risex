<?php

namespace App\Notifications;

use App\Entities\Transaction;
use App\Transformers\TransactionTransformer;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class TransactionCreated
 * @package App\Notifications
 */
class TransactionCreated extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Transaction
     */
    protected $transaction;

    /**
     * TransactionCreated constructor.
     * @param Transaction $transaction
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * @param $notifiable
     * @return array
     */
    public function toArray($notifiable) : array
    {
        return fractal($this->transaction, new TransactionTransformer)->toArray();
    }
}
