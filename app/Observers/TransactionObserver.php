<?php

namespace App\Observers;

use App\Entities\Transaction;
use App\Notifications\TransactionCreated;
use Illuminate\Support\Facades\Log;

/**
 * Class TransactionObserver
 * @package App\Observers
 */
class TransactionObserver
{

    /**
     * @param Transaction $transaction
     */
    public function created(Transaction $transaction)
    {
//        $transaction->author->notify(new TransactionCreated($transaction));
        Log::info(trans('log.transaction_created'));
    }

}