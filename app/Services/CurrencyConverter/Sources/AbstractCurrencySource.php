<?php

namespace App\Services\CurrencyConverter\Sources;

use App\Services\CurrencyConverter\CurrencyDTO;
use Curl\Curl;
use Illuminate\Support\Collection;

/**
 * Class AbstractCurrencyDriver
 * @package App\Services\CurrencyConverter\Drivers
 */
abstract class AbstractCurrencySource implements CurrencySourceInterface
{

    /**
     * @var string
     */
    protected $url;

    /**
     * @var array
     */
    protected $attributes_map;

    /**
     * AbstractCurrencyDriver constructor.
     * @param string $url
     * @param array $attributes_map
     */
    public function __construct(string $url, array $attributes_map)
    {
        $this->url = $url;
        $this->attributes_map = $attributes_map;
    }

    /**
     * @return mixed
     * @throws \ErrorException
     */
    protected function load()
    {
        return (new Curl)->get($this->url);
    }

    /**
     * @param $data
     * @return Collection
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function parse($data)
    {
        $result = [];
        foreach ($data as $currency) {
            $currency_dto = app()->make(CurrencyDTO::class);
            foreach ($this->attributes_map as $attribute => $currency_attribute) {
                $currency_dto[$attribute] = $currency[$currency_attribute];
            }
            $result[$currency_dto->getKey()] = $currency_dto;
        }
        return collect($result);
    }

    /**
     * @return Collection
     * @throws \ErrorException
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function get() : Collection
    {
        return $this->parse($this->load());
    }

}
