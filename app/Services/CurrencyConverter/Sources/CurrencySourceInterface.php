<?php

namespace App\Services\CurrencyConverter\Sources;

use Illuminate\Support\Collection;

/**
 * Interface CurrencyDriverInterface
 * @package App\Services\CurrencyConverter\Drivers
 */
interface CurrencySourceInterface
{

    /**
     * @return Collection
     */
    public function get() : Collection;

}