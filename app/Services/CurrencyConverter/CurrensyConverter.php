<?php

namespace App\Services\CurrencyConverter;

use App\Services\CurrencyConverter\Exceptions\UnknownCurrencyException;

/**
 * Class CurrencyConverter
 * @package App\Services\CurrencyConverter
 */
class CurrencyConverter
{

    /**
     * @var Courses
     */
    protected $courses;

    /**
     * CurrencyConverter constructor.
     * @param Courses $courses
     */
    public function __construct(Courses $courses)
    {
        $this->courses = $courses;
    }

    /**
     * @param string $currency_to
     * @param float $amount
     * @param string $currency_from
     * @return float|int
     * @throws \Throwable
     */
    public function convert(string $currency_to, float $amount, string $currency_from = '')
    {
        $to = $this->courses()->get($currency_to);
        throw_if(empty($to), UnknownCurrencyException::class);
        $to = $to->getValue();

        $from = $this->courses()->get($currency_from);
        $from = !empty($from) ? $from->getValue() : 1;

        return (float)$amount*((float)$from/(float)$to);
    }

    /**
     * @return Courses
     */
    protected function courses()
    {
        return $this->courses;
    }

}