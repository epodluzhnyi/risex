<?php

namespace App\Services\CurrencyConverter;

use App\Services\CurrencyConverter\Exceptions\BadCurrencyDriverConfigException;
use App\Services\CurrencyConverter\Exceptions\DefaultCurrencyDriverDoesNotSetException;
use App\Services\CurrencyConverter\Repository\CurrencyRepositoryInterface;
use App\Services\CurrencyConverter\Sources\CurrencySourceInterface;
use Illuminate\Support\ServiceProvider;

/**
 * Class CurrencyServiceProvider
 * @package App\Services\CurrencyConverter
 */
class CurrencyServiceProvider extends ServiceProvider
{

    /**
     * @throws \Exception
     */
    public function register()
    {
        $source_drivers = config('currency_converter.source_drivers');

        /**
         * Bind config drivers
         * @var array $drivers
         */
        if (!empty($source_drivers)) {
            foreach ($source_drivers as $driver => $config) {
                if (!isset($config['class']) || !isset($config['url']) || !isset($config['attributes_map'])) {
                    return new BadCurrencyDriverConfigException($driver);
                }
                $this->app->bind($config['class'] , function () use ($config) {
                    return new $config['class']($config['url'], $config['attributes_map']);
                });
            }
        }

        /**
         * Bind default driver
         */
        $default_source_driver_config = $source_drivers[config('currency_converter.default_source_driver')];
        if (empty($default_source_driver_config)) {
            throw new DefaultCurrencyDriverDoesNotSetException;
        }
        $this->app->bind(CurrencySourceInterface::class, function() use ($default_source_driver_config) {
            return $this->app->make($default_source_driver_config['class']);
        });

        /**
         * Bind currency DTO
         */
        $this->app->bind(CurrencyDTO::class, function () {
            $dto_key = config('currency_converter.currency_key');
            $dto_value_key = config('currency_converter.currency_value_key');
            $dto_attributes = config('currency_converter.currency_attributes');
            return new CurrencyDTO($dto_key, $dto_value_key, $dto_attributes);
        });

        $repository_drivers = config('currency_converter.storage_drivers');

        /**
         * Bind storage drivers
         */
        foreach ($repository_drivers as $driver) {
            $this->app->bind($driver, function() use ($driver) {
                return new $driver(config('currency_converter.currencies'));
            });
        }

        /**
         * Bind default storage
         */
        $default_storage_driver = $repository_drivers[config('currency_converter.default_storage_driver')];
        $this->app->bind(CurrencyRepositoryInterface::class, function () use ($default_storage_driver) {
            return $this->app->make($default_storage_driver);
        });

    }

}