<?php

namespace App\Services\CurrencyConverter\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class CurrencyConverter
 * @package App\Services\CurrencyConverter\Facades
 */
class CurrencyConverter extends Facade
{

    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Services\CurrencyConverter\CurrencyConverter::class;
    }

}