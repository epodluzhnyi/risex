<?php

namespace App\Services\CurrencyConverter\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Courses
 * @package App\Services\CurrencyConverter\Facades
 */
class Courses extends Facade
{

    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Services\CurrencyConverter\Courses::class;
    }

}