<?php

if (!function_exists('courses')) {

    /**
     * @return \App\Services\CurrencyConverter\Courses
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    function courses() {
        return app()->make(\App\Services\CurrencyConverter\Courses::class);
    }
}

if (!function_exists('currency_converter')) {

    /**
     * @return \App\Services\CurrencyConverter\CurrencyConverter
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    function currency_converter() {
        return app()->make(\App\Services\CurrencyConverter\CurrencyConverter::class);
    }
}