<?php

namespace App\Services\CurrencyConverter;

use App\DTO\AbstractDTO;

/**
 * Class CurrencyDTO
 * @package App\Services\CurrencyConverter
 */
class CurrencyDTO extends AbstractDTO
{

    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $value_key;

    /**
     * CurrencyDTO constructor.
     * @param string $key
     * @param string $value_key
     * @param array $attributes
     */
    public function __construct(string $key, string $value_key, array $attributes)
    {
        $this->key = $key;
        $this->value_key = $value_key;
        $this->attributes = $attributes;
    }

    /**
     * @return string
     */
    public function getKeyName()
    {
        return $this->key;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->attribute_values[$this->key];
    }

    /**
     * @return string
     */
    public function getValueKeyName()
    {
        return $this->value_key;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->attribute_values[$this->value_key];
    }

}