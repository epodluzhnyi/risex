<?php

namespace App\Services\CurrencyConverter\Repository;

use App\Services\CurrencyConverter\CurrencyDTO;
use Illuminate\Support\Collection;

/**
 * Class AbstractRepositoryDriver
 * @package App\Services\CurrencyConverter\Repository
 */
abstract class AbstractCurrencyRepository implements CurrencyRepositoryInterface
{

    /**
     * @var array
     */
    protected $currency_list;

    /**
     * AbstractRepositoryDriver constructor.
     * @param array $currency_list
     */
    public function __construct(array $currency_list)
    {
        $this->currency_list = $currency_list;
    }

    /**
     * @return Collection
     */
    public function getCollection(): Collection
    {
        $result = [];
        foreach ($this->currency_list as $currency) {
            $result[$currency] = $this->getValue($currency);
        }
        return collect($result);
    }

    /**
     * @param \ArrayAccess $collection
     * @return CurrencyRepositoryInterface
     */
    public function setCollection(\ArrayAccess $collection): CurrencyRepositoryInterface
    {
        foreach ($this->currency_list as $currency) {
            $this->setValue($collection[$currency]);
        }
        return $this;
    }

    /**
     * @param string $currency_name
     * @return CurrencyDTO | null
     */
    abstract public function getValue(string $currency_name);

    /**
     * @param CurrencyDTO $currency
     * @return CurrencyRepositoryInterface
     */
    abstract public function setValue(CurrencyDTO $currency): CurrencyRepositoryInterface;

}