<?php

namespace App\Services\CurrencyConverter\Repository;

use App\Services\CurrencyConverter\CurrencyDTO;
use Illuminate\Support\Facades\Cache;

/**
 * Class CacheStorageDriver
 * @package App\Services\CurrencyConverter\Repository
 */
class CacheRepository extends AbstractCurrencyRepository
{

    public const CACHE_TTL = 2592000;

    /**
     * @param string $currency_name
     * @return CurrencyDTO | null
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getValue(string $currency_name)
    {
        $value = null;
        if (in_array($currency_name, $this->currency_list)) {
            $data = Cache::get($currency_name);
            if (!empty($data)) {
                $value = $this->parse(json_decode($data, true));
            }
        }
        return $value;
    }

    /**
     * @param CurrencyDTO $currency
     * @return CurrencyRepositoryInterface
     */
    public function setValue(CurrencyDTO $currency): CurrencyRepositoryInterface
    {
        Cache::put($currency->getKey(), json_encode($currency->toArray()), self::CACHE_TTL);
        return $this;
    }

    /**
     * @param array $data
     * @return CurrencyDTO
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function parse(array $data) : CurrencyDTO
    {
        return app()->make(CurrencyDTO::class)->fill($data);

    }

}

