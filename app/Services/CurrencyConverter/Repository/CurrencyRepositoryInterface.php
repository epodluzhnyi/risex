<?php

namespace App\Services\CurrencyConverter\Repository;

use App\Services\CurrencyConverter\CurrencyDTO;
use Illuminate\Support\Collection;

/**
 * Interface StorageDriverInterface
 * @package App\Services\CurrencyConverter\Storages
 */
interface CurrencyRepositoryInterface
{

    /**
     * @param \ArrayAccess $collection
     * @return CurrencyRepositoryInterface
     */
    public function setCollection(\ArrayAccess $collection) : CurrencyRepositoryInterface;

    /**
     * @return Collection
     */
    public function getCollection() : Collection;

    /**
     * @param CurrencyDTO $currency
     * @return CurrencyRepositoryInterface
     */
    public function setValue(CurrencyDTO $currency) : CurrencyRepositoryInterface;

    /**
     * @param string $currency_name
     * @return CurrencyDTO | null
     */
    public function getValue(string $currency_name);

}