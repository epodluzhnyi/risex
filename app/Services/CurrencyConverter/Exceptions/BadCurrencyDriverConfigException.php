<?php

namespace App\Services\CurrencyConverter\Exceptions;

use Throwable;

/**
 * Class BadCurrencyDriverConfigException
 * @package App\Services\CurrencyConverter
 */
class BadCurrencyDriverConfigException extends \Exception
{

    /**
     * BadCurrecyDriverConfigException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf('Bad currency driver config for the %s driver.', $message), $code, $previous);
    }

}