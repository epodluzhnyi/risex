<?php

namespace App\Services\CurrencyConverter\Exceptions;

/**
 * Class DefaultCurrencyDriverDoesNotSetException
 * @package App\Services\CurrencyConverter
 */
class DefaultCurrencyDriverDoesNotSetException extends \Exception
{

    /**
     * @var string
     */
    protected $message = 'Default currency driver does not set';

}