<?php

namespace App\Services\CurrencyConverter\Exceptions;

use Throwable;

/**
 * Class UnknownCurrencyException
 * @package App\Services\CurrencyConverter\Exceptions
 */
class UnknownCurrencyException extends \Exception
{

    /**
     * UnknownCurrencyException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf('Unknown currency %s', $message), $code, $previous);
    }

}