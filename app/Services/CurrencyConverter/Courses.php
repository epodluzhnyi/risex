<?php

namespace App\Services\CurrencyConverter;

use App\Services\CurrencyConverter\Repository\CurrencyRepositoryInterface;
use App\Services\CurrencyConverter\Sources\CurrencySourceInterface;

/**
 * Class Courses
 * @package App\Services\CurrencyConverter
 */
class Courses
{

    /**
     * @var CurrencySourceInterface
     */
    protected $source;

    /**
     * @var CurrencyRepositoryInterface
     */
    protected $repository;

    /**
     * Courses constructor.
     * @param CurrencySourceInterface $source
     * @param CurrencyRepositoryInterface $repository
     */
    public function __construct(CurrencySourceInterface $source, CurrencyRepositoryInterface $repository)
    {
        $this->source = $source;
        $this->repository = $repository;
    }

    /**
     * @return $this
     */
    public function updateCache()
    {
        $this->repository()->setCollection($this->source->get());
        return $this;
    }

    /**
     * @param string $currency
     * @return CurrencyDTO|null
     */
    public function get(string $currency)
    {
        return $this->repository()->getValue($currency);
    }

    /**
     * @return CurrencySourceInterface
     */
    public function source() : CurrencySourceInterface
    {
        return $this->source;
    }

    /**
     * @return CurrencyRepositoryInterface
     */
    public function repository() : CurrencyRepositoryInterface
    {
        return $this->repository;
    }

}