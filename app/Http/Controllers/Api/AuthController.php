<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\JwtTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class AuthController
 * @package App\Http\Controllers\Api
 */
class AuthController extends Controller
{

    /**
     * @return mixed
     */
    public function guard()
    {
        return Auth::guard();
    }

    /**
     * Sign in
     * @bodyParam email string required user email
     * @bodyParam password string required user password
     * @response {
     *  "data": {
     *      "access_token": "your_access_token",
     *      "token_type": "bearer"
     *  }
     * }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->all('email', 'password');

        if ($token = $this->guard()->attempt($credentials)) {
            return fractal($token, new JwtTransformer());
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }

    /**
     * Refresh jwt-token
     * @queryParam Authorization:Bearer required You can add query parameter "Authorization: Bearer" er header Authorization:Bearer. Old jwt-token.
     * @response {
     *  "data": {
     *      "access_token": "your_access_token",
     *      "token_type": "bearer"
     *  }
     * }
     *
     * @return \Spatie\Fractal\Fractal
     */
    public function refreshToken()
    {
        return fractal(auth()->refresh(), new JwtTransformer());
    }

}