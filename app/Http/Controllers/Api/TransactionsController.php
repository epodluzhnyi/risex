<?php

namespace App\Http\Controllers\Api;

use App\Entities\Transaction;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\TransactionSaveRequest;
use App\QueryFilters\TransactionFilters;
use App\Transformers\TransactionTransformer;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractal\Fractal;

/**
 * Class TransactionsController
 * @package App\Http\Controllers\Api
 */
class TransactionsController extends Controller
{

    /**
     * Paginated list of transactions
     * @queryParam amountMore Filter by the amount more than parameter value
     * @queryParam amountLess Filter by the amount less than parameter value
     * @queryParam income Returns just incoming transactions
     * @queryParam consumption Returns just consumption transactions
     * @queryParam dateFrom Date in "Y-m-d" format. Filter by the creating date later than parameter value
     * @queryParam dateTo Date in "Y-m-d" format. Filter by the creating date earlier than parameter value
     * @headers {
     *  "Authorization": "Bearer you_access_token"
     * }
     * @response {
     *  "data": [
     *      {
     *          "id": 124,
     *          "amount": -1000,
     *          "title": "Перевод №1",
     *          "created_at": 1553146200,
     *          "updated_at": 1553146200,
     *          "author": {
     *              "data": {
     *                  "id": 1,
     *                  "name": "Evgeny",
     *                  "email": "e.podluzhnyi@gmail.com",
     *                  "created_at": 1553121604,
     *                  "updated_at": 1553717138
     *              }
     *          }
     *      }
     * ]
     * }
     *
     * @param TransactionFilters $filters
     * @return mixed
     */
    public function index(TransactionFilters $filters)
    {
        $paginator = Transaction::filterBy($filters)->paginate();
        $transactions = $paginator->getCollection();
        return
            \fractal()
                ->collection($transactions)
                ->transformWith(new TransactionTransformer)
                ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ;
    }

    /**
     * Create new transaction
     * @bodyParam amount int required Transaction sum
     * @bodyParam title string required Transaction name
     * @response {
     *  "data": {
     *      "id": 87238873,
     *      "amount": 1100,
     *      "title": "Transaction title",
     *      "created_at": 1553472000,
     *      "updated_at": 1553472000,
     *      "author": {
     *          "id": 23456,
     *          "name": "User name",
     *          "email": "user@email.com",
     *          "created_at": 1553472000,
     *          "updated_at": 1553472000
     *      }
     *  }
     * }
     *
     * @param TransactionSaveRequest $request
     * @return Fractal
     */
    public function store(TransactionSaveRequest $request)
    {
        $transaction = new Transaction;
        $transaction->fill($request->only($transaction->getFillable()));
        /** @var User $user */
        $user = Auth::user();
        $transaction = $user->transactions()->save($transaction);
        return \fractal($transaction, new TransactionTransformer);
    }

    /**
     * Get current transaction
     * @queryParam transaction required id of required transaction
     * @response {
     *  "data": [
     *      {
     *          "id": 124,
     *          "amount": -1000,
     *          "title": "Перевод №1",
     *          "created_at": 1553146200,
     *          "updated_at": 1553146200,
     *          "author": {
     *              "data": {
     *                  "id": 1,
     *                  "name": "Evgeny",
     *                  "email": "e.podluzhnyi@gmail.com",
     *                  "created_at": 1553121604,
     *                  "updated_at": 1553717138
     *              }
     *          }
     *      }
     * ]
     * }
     *
     * @param Transaction $transaction
     * @return Fractal
     */
    public function show(Transaction $transaction)
    {
        return \fractal($transaction, new TransactionTransformer);
    }

    /**
     * Update transaction
     * @queryParam transaction required id of required transaction
     * @bodyParam amount int required Transaction sum
     * @bodyParam title string required Transaction name
     * @response {
     *  "data": [
     *      {
     *          "id": 124,
     *          "amount": -1000,
     *          "title": "Перевод №1",
     *          "created_at": 1553146200,
     *          "updated_at": 1553146200,
     *          "author": {
     *              "data": {
     *                  "id": 1,
     *                  "name": "Evgeny",
     *                  "email": "e.podluzhnyi@gmail.com",
     *                  "created_at": 1553121604,
     *                  "updated_at": 1553717138
     *              }
     *          }
     *      }
     * ]
     * }
     *
     * @param TransactionSaveRequest $request
     * @param $id
     * @return Fractal
     */
    public function update(TransactionSaveRequest $request, $id)
    {
        abort_if($transaction = Transaction::find($id), 404);
        abort_if($transaction->autor->getKey() != $request->user()->getKey(), 403);
        $transaction
            ->fill(
                $request->only(
                    $transaction->getFillable()
                )
            )
            ->save()
        ;
        return \fractal($transaction, new TransactionTransformer);
    }

    /**
     * Remove the specified transaction
     * @queryParam transaction required id of required transaction
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort_if($transaction = Transaction::find($id), 404);
        $transaction->delete();
        return response(trans('api.transaction_deleted'), 200);
    }

}
