<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\QueryFilters\TransactionFilters;
use App\Services\CurrencyConverter\Exceptions\UnknownCurrencyException;
use App\Transformers\AmountTransformer;
use App\Transformers\TransactionTransformer;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{

    /**
     * @var mixed
     */
    protected $user;

    /**
     * UserController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    /**
     * Get filtered transactions or sum of transactions of current user
     * @queryParam amountMore Filter by the amount more than parameter value
     * @queryParam amountLess Filter by the amount less than parameter value
     * @queryParam income Returns just incoming transactions
     * @queryParam consumption Returns just consumption transactions
     * @queryParam dateFrom Date in "Y-m-d" format. Filter by the creating date later than parameter value
     * @queryParam dateTo Date in "Y-m-d" format. Filter by the creating date earlier than parameter value
     * @queryParam amount Calculates the sum of all filtered transactions
     * @headers {
     *  "Authorization": "Bearer you_access_token"
     * }
     * @response {
     *  "data": {
     *      "amount": 4536768
     *  }
     * }
     *
     * @param TransactionFilters $filters
     * @param Request $request
     * @return \Spatie\Fractal\Fractal
     */
    public function transactions(TransactionFilters $filters, Request $request)
    {
        $builder = $this->user->transactions()->filterBy($filters);
        return
            $request->has('amount')
                ? fractal($builder->sum('amount'), new AmountTransformer)
                : fractal($builder->get(), new TransactionTransformer)
            ;
    }

    /**
     * Get user balance
     * @queryParam currency This is char code of the required currency
     * @response {
     *  "data": {
     *      "amount": 4536768.45678
     *  }
     * }
     *
     * @param Request $request
     * @return \Spatie\Fractal\Fractal
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Throwable
     */
    public function balance(Request $request)
    {
        try {
            $balance = $this->user->transactions()->sum('amount');
            $currency = $request->get('currency');
            $balance =
                !empty($currency)
                    ? currency_converter()->convert($currency, $balance)
                    : $balance;
            return fractal($balance, new AmountTransformer);
        } catch (UnknownCurrencyException $e) {
            abort(404, $e->getMessage());
        }

    }

}
