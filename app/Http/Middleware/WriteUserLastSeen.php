<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Class WriteUserLastSeen
 * @package App\Http\Middleware
 */
class WriteUserLastSeen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            Auth::user()->setAttribute('last_seen', Carbon::now())->save();
        }
        return $next($request);
    }
}
