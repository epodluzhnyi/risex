<?php

namespace App\QueryFilters;

use Carbon\Carbon;
use Cerbero\QueryFilters\QueryFilters;

/**
 * Class TransactionFilters
 * @package App\QueryFilters
 */
class TransactionFilters extends QueryFilters
{

    /**
     * @param int $amount
     * @return void
     */
    public function amountMore(int $amount)
    {
        $this->query->where('amount', '>', $amount);
    }

    /**
     * @param int $amount
     * @return void
     */
    public function amountLess(int $amount)
    {
        $this->query->where('amount', '<', $amount);
    }

    /**
     * @return void
     */
    public function income()
    {
        $this->query->where('amount', '>', 0);
    }

    /**
     * @return void
     */
    public function consumption()
    {
        $this->query->where('amount', '<', 0);
    }

    /**
     * @param string $date
     * @return void
     */
    public function dateFrom(string $date)
    {
        $this->query->where('created_at', '>', Carbon::parse($date)->format('Y-m-d H:i:s'));
    }

    /**
     * @param string $date
     * @return void
     */
    public function dateTo(string $date)
    {
        $this->query->where('created_at', '<', Carbon::parse($date)->format('Y-m-d H:i:s'));
    }

}
