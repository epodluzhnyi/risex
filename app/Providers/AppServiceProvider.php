<?php

namespace App\Providers;

use App\Entities\Transaction;
use App\Observers\TransactionObserver;
use App\Services\CurrencyConverter\Sources\CurrencySourceInterface;
use App\Services\CurrencyConverter\Sources\JSONCurrencySource;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->extend(\App\Services\CurrencyConverter\Drivers\AbstractCurrencyDriver::class, function ($app, $driver) {
//            throw new \Exception('wtf???');
//
//            $classname = '\App\Services\CurrencyConverter\Drivers\CurrencyDriverInterface';
//            return new $classname(config('currency_converter'));
//        });

//        $this->app->bind(XMLCurrencyDriver::class, function () {
//            throw new \Exception('wtf ololo');
//        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Transaction::observe(TransactionObserver::class);
    }
}
