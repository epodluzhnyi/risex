<?php

return [

    /**
     * Cache ttl
     */
    'cache_ttl' => 2592000,

    /**
     * List of available currencies
     */
    'currencies' => [
        'AUD',
        'AZN',
        'GBP',
        'AMD',
        'BYN',
        'BGN',
        'BRL',
        'HUF',
        'HKD',
        'DKK',
        'USD',
        'EUR',
        'INR',
        'KZT',
        'CAD',
        'KGS',
        'CNY',
        'MDL',
        'NOK',
        'PLN',
        'RON',
        'XDR',
        'SGD',
        'TJS',
        'TRY',
        'TMT',
        'UZS',
        'UAH',
        'CZK',
        'SEK',
        'CHF',
        'ZAR',
        'KRW',
        'JPY',
    ],

    /**
     * Currency primary key
     */
    'currency_key' => 'char_code',

    /**
     * Currency value key
     */
    'currency_value_key' => 'value',

    /**
     * List of currency DTO attributes
     */
    'currency_attributes' => [
        'num_code',
        'char_code',
        'value',
        'old_value',
    ],

    /**
     * This is a driver, which will use by default
     */
    'default_source_driver' => 'json',

    /**
     * List of the all drivers and map of driver currency property to service currency attributes
     */
    'source_drivers' => [
        'xml' => [
            'class' => \App\Services\CurrencyConverter\Sources\XMLCurrencySource::class,
            'url' => env('COURSES_XML_URL', null),
            'attributes_map' => [
                'num_code'  => 'NumCode',
                'char_code' => 'CharCode',
                'value'     => 'Value',
            ],
        ],
        'json' => [
            'class' => \App\Services\CurrencyConverter\Sources\JSONCurrencySource::class,
            'url' => env('COURSES_JSON_URL', null),
            'attributes_map' => [
                'num_code'  => 'NumCode',
                'char_code' => 'CharCode',
                'value'     => 'Value',
                'old_value' => 'Previous',
            ],
        ],
        'csv' => [
            'class' => \App\Services\CurrencyConverter\Sources\CSVCurrencySource::class,
            'url' => env('COURSES_CSV_URL', null),
            'attributes_map' => [
                'num_code'  => '|__NumCode',
                'char_code' => '|__CharCode',
                'value'     => '|__Value',
                'old_value' => '|__Previous',
            ],
        ],
        'average' => [
            'class' => \App\Services\CurrencyConverter\Sources\AveragedCurrencySource::class,
            'url' => '',
            'attributes_map' => [
                'num_code'  => 'NumCode',
                'char_code' => 'CharCode',
                'value'     => 'Value',
                'old_value' => 'Previous',
            ],
        ],
    ],

    /**
     * Default storage driver
     */
    'default_storage_driver' => 'cache',

    /**
     * Available storage drivers
     */
    'storage_drivers' => [
        'cache' => \App\Services\CurrencyConverter\Repository\CacheRepository::class,
    ],

];