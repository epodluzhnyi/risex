---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>

---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://risex.podluzhnyi.ru/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## Sign in

> Example request:

```bash
curl -X POST "http://risex.podluzhnyi.ru/api/login" \
    -H "Content-Type: application/json" \
    -d '{"email":"PoBdzBAzdoTKsmE4","password":"Q0SJzrg6CPGQTKgg"}'

```

```javascript
const url = new URL("http://risex.podluzhnyi.ru/api/login");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "email": "PoBdzBAzdoTKsmE4",
    "password": "Q0SJzrg6CPGQTKgg"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "access_token": "your_access_token",
        "token_type": "bearer"
    }
}
```

### HTTP Request
`POST api/login`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | string |  required  | user email
    password | string |  required  | user password

<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->


<!-- START_1269f35509154db080ef2a7e00fce890 -->
## Refresh jwt-token

> Example request:

```bash
curl -X GET -G "http://risex.podluzhnyi.ru/api/token/refresh" 
```

```javascript
const url = new URL("http://risex.podluzhnyi.ru/api/token/refresh");

    let params = {
            "Authorization:Bearer": "9QfRjOawtdu7ekp9",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "access_token": "your_access_token",
        "token_type": "bearer"
    }
}
```

### HTTP Request
`GET api/token/refresh`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    Authorization:Bearer |  required  | You can add query parameter "Authorization: Bearer" er header Authorization:Bearer. Old jwt-token.

<!-- END_1269f35509154db080ef2a7e00fce890 -->


<!-- START_9af0b9f04f16a1c9705c5300772f6f16 -->
## Paginated list of transactions

> Example request:

```bash
curl -X GET -G "http://risex.podluzhnyi.ru/api/transactions" 
```

```javascript
const url = new URL("http://risex.podluzhnyi.ru/api/transactions");

    let params = {
            "amountMore": "scc0Vaqq53jWcyLH",
            "amountLess": "9QxBdhcb7ljpwBVJ",
            "income": "6jRDB7kQbGjCAFCP",
            "consumption": "qMxNVN60LUfiPbjr",
            "dateFrom": "NtG1CyiCsYXuXGe3",
            "dateTo": "0fjclfW1h7Os2aLq",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 124,
            "amount": -1000,
            "title": "Перевод №1",
            "created_at": 1553146200,
            "updated_at": 1553146200,
            "author": {
                "data": {
                    "id": 1,
                    "name": "Evgeny",
                    "email": "e.podluzhnyi@gmail.com",
                    "created_at": 1553121604,
                    "updated_at": 1553717138
                }
            }
        }
    ]
}
```

### HTTP Request
`GET api/transactions`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    amountMore |  optional  | Filter by the amount more than parameter value
    amountLess |  optional  | Filter by the amount less than parameter value
    income |  optional  | Returns just incoming transactions
    consumption |  optional  | Returns just consumption transactions
    dateFrom |  optional  | Date in "Y-m-d" format. Filter by the creating date later than parameter value
    dateTo |  optional  | Date in "Y-m-d" format. Filter by the creating date earlier than parameter value

<!-- END_9af0b9f04f16a1c9705c5300772f6f16 -->


<!-- START_a524d236dd691776be3315d40786a1db -->
## Create new transaction

> Example request:

```bash
curl -X POST "http://risex.podluzhnyi.ru/api/transactions" \
    -H "Content-Type: application/json" \
    -d '{"amount":2,"title":"U7R9cJoKA3wDWPJQ"}'

```

```javascript
const url = new URL("http://risex.podluzhnyi.ru/api/transactions");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "amount": 2,
    "title": "U7R9cJoKA3wDWPJQ"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 87238873,
        "amount": 1100,
        "title": "Transaction title",
        "created_at": 1553472000,
        "updated_at": 1553472000,
        "author": {
            "id": 23456,
            "name": "User name",
            "email": "user@email.com",
            "created_at": 1553472000,
            "updated_at": 1553472000
        }
    }
}
```

### HTTP Request
`POST api/transactions`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    amount | integer |  required  | Transaction sum
    title | string |  required  | Transaction name

<!-- END_a524d236dd691776be3315d40786a1db -->


<!-- START_b0a0c604cbe4ea6a880e51244ea30727 -->
## Get current transaction

> Example request:

```bash
curl -X GET -G "http://risex.podluzhnyi.ru/api/transactions/{transaction}" 
```

```javascript
const url = new URL("http://risex.podluzhnyi.ru/api/transactions/{transaction}");

    let params = {
            "transaction": "R9eNhqKov1zayoDn",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 124,
            "amount": -1000,
            "title": "Перевод №1",
            "created_at": 1553146200,
            "updated_at": 1553146200,
            "author": {
                "data": {
                    "id": 1,
                    "name": "Evgeny",
                    "email": "e.podluzhnyi@gmail.com",
                    "created_at": 1553121604,
                    "updated_at": 1553717138
                }
            }
        }
    ]
}
```

### HTTP Request
`GET api/transactions/{transaction}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    transaction |  required  | id of required transaction

<!-- END_b0a0c604cbe4ea6a880e51244ea30727 -->


<!-- START_507fdb7fd111d325f1e65ea3af991828 -->
## Update transaction

> Example request:

```bash
curl -X PUT "http://risex.podluzhnyi.ru/api/transactions/{transaction}" \
    -H "Content-Type: application/json" \
    -d '{"amount":1,"title":"fnvVlfYuBq3b4QUg"}'

```

```javascript
const url = new URL("http://risex.podluzhnyi.ru/api/transactions/{transaction}");

    let params = {
            "transaction": "LGWNjAe4BKcizWvz",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "amount": 1,
    "title": "fnvVlfYuBq3b4QUg"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 124,
            "amount": -1000,
            "title": "Перевод №1",
            "created_at": 1553146200,
            "updated_at": 1553146200,
            "author": {
                "data": {
                    "id": 1,
                    "name": "Evgeny",
                    "email": "e.podluzhnyi@gmail.com",
                    "created_at": 1553121604,
                    "updated_at": 1553717138
                }
            }
        }
    ]
}
```

### HTTP Request
`PUT api/transactions/{transaction}`

`PATCH api/transactions/{transaction}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    amount | integer |  required  | Transaction sum
    title | string |  required  | Transaction name
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    transaction |  required  | id of required transaction

<!-- END_507fdb7fd111d325f1e65ea3af991828 -->


<!-- START_98b2138fd216945a4b16f2764f45d261 -->
## Remove the specified transaction

> Example request:

```bash
curl -X DELETE "http://risex.podluzhnyi.ru/api/transactions/{transaction}" 
```

```javascript
const url = new URL("http://risex.podluzhnyi.ru/api/transactions/{transaction}");

    let params = {
            "transaction": "hPrj4q1hgu1F2f2c",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/transactions/{transaction}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    transaction |  required  | id of required transaction

<!-- END_98b2138fd216945a4b16f2764f45d261 -->


<!-- START_3202eea1b51e6034f103a06a3bed0306 -->
## Get filtered transactions or sum of transactions of current user

> Example request:

```bash
curl -X GET -G "http://risex.podluzhnyi.ru/api/user/transactions" 
```

```javascript
const url = new URL("http://risex.podluzhnyi.ru/api/user/transactions");

    let params = {
            "amountMore": "wAz5niLPoiGT3dF8",
            "amountLess": "Cn45f8cSqHIaDZf5",
            "income": "Pu5PCavxgeXNBRrE",
            "consumption": "GEuF4ZS2Ha7qpdNA",
            "dateFrom": "sT0HvrootIJ4Owlv",
            "dateTo": "CJeiUWDDZlVOC3KQ",
            "amount": "WKVYDWPKzP5lEztp",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "amount": 4536768
    }
}
```

### HTTP Request
`GET api/user/transactions`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    amountMore |  optional  | Filter by the amount more than parameter value
    amountLess |  optional  | Filter by the amount less than parameter value
    income |  optional  | Returns just incoming transactions
    consumption |  optional  | Returns just consumption transactions
    dateFrom |  optional  | Date in "Y-m-d" format. Filter by the creating date later than parameter value
    dateTo |  optional  | Date in "Y-m-d" format. Filter by the creating date earlier than parameter value
    amount |  optional  | Calculates the sum of all filtered transactions

<!-- END_3202eea1b51e6034f103a06a3bed0306 -->


<!-- START_a1f2cfd9ee214b693a24337d28528102 -->
## Get user balance

> Example request:

```bash
curl -X GET -G "http://risex.podluzhnyi.ru/api/user/balance/{currency?}" 
```

```javascript
const url = new URL("http://risex.podluzhnyi.ru/api/user/balance/{currency?}");

    let params = {
            "currency": "1UWh0RRxeJgllNQc",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "amount": 4536768.45678
    }
}
```

### HTTP Request
`GET api/user/balance/{currency?}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    currency |  optional  | This is char code of the required currency

<!-- END_a1f2cfd9ee214b693a24337d28528102 -->




